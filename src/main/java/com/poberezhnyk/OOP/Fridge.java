package com.poberezhnyk.OOP;

public class Fridge extends HomeAppliance {

    public Fridge() {
    }

    public Fridge(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("Fridge " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("Fridge " + getName() + " turned off" );
    }
}
