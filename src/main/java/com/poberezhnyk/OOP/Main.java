package com.poberezhnyk.OOP;

import com.poberezhnyk.OOP.Comparator.PowerConsumptionComparator;
import com.poberezhnyk.OOP.Enum.HomeAppliancesEnum;
import com.poberezhnyk.OOP.Enum.MenuEnum;
import com.poberezhnyk.OOP.Exception.ApplianceTurnedOnException;
import com.poberezhnyk.OOP.Exception.NoSuchApplianceException;
import com.poberezhnyk.OOP.Exception.WrongActionException;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<HomeAppliance> homeAppliances = new ArrayList<>();
        HomeAppliancesManager siri = new HomeAppliancesManager("Siri", homeAppliances);
        siri.addNewHomeApplianceToArray(new TV(50, "LG"));
        siri.addNewHomeApplianceToArray(new CoffeeMachine(1450, "DeLonghi ECAM1"));
        siri.addNewHomeApplianceToArray(new CoffeeMachine(1250, "DeLonghi ECAM2"));
        siri.addNewHomeApplianceToArray(new Laptop(120, "Lenovo Y740"));
        siri.addNewHomeApplianceToArray(new Computer(400, "CustomPC"));
        siri.addNewHomeApplianceToArray(new Microwave(1150, "Samsung "));
        System.out.println();
        System.out.println("Welcome home. I'm your power consumption helper, " + siri.getHomeManagerName() + ".");
        MenuEnum menuOptions = MenuEnum.values()[0];
        Scanner input = new Scanner(System.in);
        while (menuOptions != MenuEnum.values()[MenuEnum.values().length - 1]) {
            try {
                printMenu();
                int val = Integer.parseInt(input.nextLine().trim());
                menuOptions = MenuEnum.values()[val];
                switch (menuOptions) {
                    case a: {
                        siri.getHomeAppliances().
                                forEach(ha -> System.out.println(ha.getClass()
                                        + ", name = " + ha.getName() + ", power consumption = " + ha.getConsumption()));
                    }
                    break;
                    case b: {
                        siri.findAllEnabledAppliances().
                                forEach(ha -> System.out.println(ha.getClass()
                                        + ", name = " + ha.getName() + ", power consumption = " + ha.getConsumption()));
                    }
                    break;
                    case c: {
                        System.out.println("Total power consumption = " + siri.getPowerConsumption() + "W");
                    }
                    break;
                    case d: {

                        try {
                            siri.addNewHomeApplianceToArray(createNewHomeAppliance());
                        } catch (WrongActionException e) {
                            break;
                        }
                    }
                    break;
                    case e: {
                        try {
                            HomeAppliance homeAppliance = chooseOneFromAppliancesListByClass(siri.getHomeAppliances());
                            siri.removeHomeApplianceFromArray(siri.findChosenAppliance(homeAppliance));
                        } catch (NullPointerException e) {
                            System.out.println("Empty category");
                        } catch (NoSuchApplianceException | ApplianceTurnedOnException e) {
                            e.printStackTrace();
                        } catch (WrongActionException e) {
                            break;
                        }
                    }
                    break;
                    case f: {
                        try {
                            HomeAppliance homeAppliance = chooseOneFromAppliancesListByClass(siri.getHomeAppliances());
                            siri.findChosenAppliance(homeAppliance).turnOn();
                        } catch (NullPointerException e) {
                            System.out.println("Empty category");
                        } catch (WrongActionException e) {
                            break;
                        }
                    }
                    break;
                    case g: {
                        try {
                            HomeAppliance homeAppliance = chooseOneFromEnabledHomeAppliances(siri.findAllEnabledAppliances());
                            siri.findChosenAppliance(homeAppliance).turnOff();
                        } catch (NullPointerException e) {
                            System.out.println("Empty category");
                        } catch (WrongActionException e) {
                            break;
                        }
                    }
                    break;
                    case h: {
                        try {
                            ArrayList<HomeAppliance> enabledAppliances = siri.findAllEnabledAppliances();
                            Collections.sort(enabledAppliances, new PowerConsumptionComparator());
                            enabledAppliances.forEach(enabledAppliance -> System.out.println(enabledAppliance));
                        } catch (NullPointerException e) {
                            System.out.println("Empty category");
                        }
                    }
                    break;
                    case q:
                        System.out.println("Goodbye.");
                        break;
                    default:
                        System.out.println("Selection out of range. Try again");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Selection out of range. Try again:");
            }
        }
    }

    public static void printMenu() {
        System.out.println("\nChoose what to do:");
        System.out.println("1. Show home appliances.");
        System.out.println("2. Show enabled home appliances.");
        System.out.println("3. Show power consumption.");
        System.out.println("4. Add new appliance.");
        System.out.println("5. Remove appliance.");
        System.out.println("6. Turn on appliance.");
        System.out.println("7. Turn off appliance.");
        System.out.println("8. Sort by consumption");
        System.out.println("9. Quit.");
        System.out.print("\nSelection -> ");
    }

    public static void printAppliancesEnumValuesMenu() {
        System.out.println("Choose type:");
        for (int i = 0; i < HomeAppliancesEnum.values().length; i++) {
            System.out.println("\t" + (i + 1) + ". " + HomeAppliancesEnum.values()[i]);
        }
        System.out.println("\t" + (HomeAppliancesEnum.values().length + 1) + "." + "Back to main menu.");
        System.out.print("\nSelection -> ");
    }

    public static void printArrayValuesMenu(ArrayList<HomeAppliance> homeAppliances) {
        System.out.println("Choose appliance: ");
        for (int i = 0; i < homeAppliances.size(); i++) {
            System.out.println("\t" + (i + 1) + "." + homeAppliances.get(i));
        }
        System.out.println("\t" + (homeAppliances.size() + 1) + "." + "Back to main menu.");
        System.out.print("\nSelection -> ");
    }


    public static ArrayList<HomeAppliance> findAppliancesListByClass(ArrayList<HomeAppliance> homeAppliances) throws
            WrongActionException {
        printAppliancesEnumValuesMenu();
        int applianceClassFromMenu=enterValue(HomeAppliancesEnum.values().length);
        ArrayList<HomeAppliance> chosenClassApplianceList = null;
        switch (applianceClassFromMenu) {
            case 1:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new CoffeeMachine());
                break;
            case 2:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new Computer());
                break;
            case 3:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new Fridge());
                break;
            case 4:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new Laptop());
                break;
            case 5:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new Microwave());
                break;
            case 6:
                chosenClassApplianceList = HomeAppliancesManager.
                        findAppliancesByClass(homeAppliances, new TV());
                break;
            case 7:
                throw new WrongActionException();
        }
        return chosenClassApplianceList;
    }

    public static HomeAppliance chooseOneFromAppliancesListByClass(ArrayList<HomeAppliance> homeAppliances) throws
            WrongActionException {
        ArrayList<HomeAppliance> AppliancesListByClass = findAppliancesListByClass(homeAppliances);
        if (AppliancesListByClass.size() == 0) {
            return null;
        }
        printArrayValuesMenu(AppliancesListByClass);
        int chosenApplianceIndex=enterValue(AppliancesListByClass.size());
        return AppliancesListByClass.get(chosenApplianceIndex - 1);
    }

    public static HomeAppliance createNewHomeAppliance() throws WrongActionException {
        Scanner input = new Scanner(System.in);
        printAppliancesEnumValuesMenu();
        int applianceClassFromMenu=enterValue(HomeAppliancesEnum.values().length);
        System.out.println("Enter appliance name:");
        input.nextLine();
        String name = input.nextLine();
        System.out.println("Enter power consumption:");
        int consumption = 0;
        try {
            consumption = input.nextInt();
            while (consumption <= 0) {
                System.out.println("Consumption can't be less or equal zero. Enter again:");
                System.out.print("\nSelection -> ");
                consumption = input.nextInt();
            }
        } catch (InputMismatchException e) {
            System.out.println("Wrong input. Back to main menu");
            throw new WrongActionException();
        }
        System.out.println("type " + applianceClassFromMenu);
        switch (applianceClassFromMenu) {
            case 1:
                return new CoffeeMachine(consumption, name);
            case 2:
                return new Computer(consumption, name);
            case 3:
                return new Fridge(consumption, name);
            case 4:
                return new Laptop(consumption, name);
            case 5:
                return new Microwave(consumption, name);
            case 6:
                return new TV(consumption, name);
        }
        return null;
    }

    public static HomeAppliance chooseOneFromEnabledHomeAppliances(ArrayList<HomeAppliance> homeAppliances) throws
            WrongActionException {
        printArrayValuesMenu(homeAppliances);
        int chosenApplianceIndex=enterValue(homeAppliances.size());
        return homeAppliances.get(chosenApplianceIndex - 1);
    }

    public static int enterValue(int arraySize) throws WrongActionException {
        Scanner input = new Scanner(System.in);
        int value;
        try {
            value = input.nextInt();
            while (value < 0 || value > (arraySize + 1)) {
                System.out.println("Wrong input number. Enter again:");
                System.out.print("\nSelection -> ");
                value = input.nextInt();
            }
        } catch (InputMismatchException e) {
            System.out.println("Input error. Back to main menu");
            throw new WrongActionException();
        }
        if (value == (arraySize + 1)) {
            throw new WrongActionException();
        }
        return value;
    }
}
