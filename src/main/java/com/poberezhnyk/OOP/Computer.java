package com.poberezhnyk.OOP;

public class Computer extends HomeAppliance {

    public Computer() {
    }

    public Computer(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("Computer " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("Computer " + getName() + " turned off" );
    }

}
