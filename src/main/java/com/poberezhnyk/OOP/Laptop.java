package com.poberezhnyk.OOP;

public class Laptop extends HomeAppliance {

    public Laptop() {
    }

    public Laptop(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("Laptop " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("Laptop " + getName() + " turned off" );
    }

}
