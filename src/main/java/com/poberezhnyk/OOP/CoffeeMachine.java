package com.poberezhnyk.OOP;

public class CoffeeMachine extends HomeAppliance {

    public CoffeeMachine() {
    }

    public CoffeeMachine(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("CoffeeMachine " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("CoffeeMachine " + getName() + " turned off" );
    }
}
