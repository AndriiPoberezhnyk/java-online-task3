package com.poberezhnyk.OOP.Enum;

public enum MenuEnum {
    z("Continue"), a("Show home appliances"), b("Show enabled home appliances"), c("Show power consumption"),
    d("Add new consumer"), e("Remove consumer"), f("Turn on appliance"), g("Turn off appliance"),h("Sort and show by..."), q("Quit");

    private String meaning;

    MenuEnum(String meaning) {
        this.meaning = meaning;
    }

    public String getMeaning() {
        return meaning;
    }
}
