package com.poberezhnyk.OOP.Enum;

public enum HomeAppliancesEnum {
    CoffeeMachine, Computer, Fridge, Laptop, Microwave, TV;
}
