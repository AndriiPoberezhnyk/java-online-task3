package com.poberezhnyk.OOP;

import java.util.Objects;

public abstract class HomeAppliance {
    private boolean isTurnedOn = false;
    private int consumption;
    private String name;

    public HomeAppliance() {
    }

    public HomeAppliance(int consumption, String name) {
        this.consumption = consumption;
        this.name = name;
    }

    abstract void turnOn();

    abstract void turnOff();

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    public void setTurnedOn(boolean turnedOn) {
        isTurnedOn = turnedOn;
    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HomeAppliance that = (HomeAppliance) o;
        return consumption == that.consumption &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consumption, name);
    }

    @Override
    public String toString() {
        return "HomeAppliance{" +
                "consumption=" + consumption +
                ", name='" + name + '\'' +
                '}';
    }


}
