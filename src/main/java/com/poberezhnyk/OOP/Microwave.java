package com.poberezhnyk.OOP;

public class Microwave extends HomeAppliance {

    public Microwave() {
    }

    public Microwave(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("Microwave " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("Microwave " + getName() + " turned off" );
    }

}
