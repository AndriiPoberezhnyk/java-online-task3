package com.poberezhnyk.OOP.Comparator;

import com.poberezhnyk.OOP.HomeAppliance;

import java.util.Comparator;

public class PowerConsumptionComparator implements Comparator<HomeAppliance> {

    @Override
    public int compare(HomeAppliance o1, HomeAppliance o2) {
        if (o1.getConsumption() > o2.getConsumption()) {
            return 1;
        } else if (o1.getConsumption() < o2.getConsumption()) {
            return -1;
        } else if (o1.getConsumption() == o2.getConsumption()) {
            return o1.getName().compareTo(o2.getName());
        }
        return 0;
    }
}
