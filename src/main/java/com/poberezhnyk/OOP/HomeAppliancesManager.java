package com.poberezhnyk.OOP;

import com.poberezhnyk.OOP.Exception.ApplianceTurnedOnException;
import com.poberezhnyk.OOP.Exception.NoSuchApplianceException;

import java.util.ArrayList;

public class HomeAppliancesManager {
    private String homeManagerName;
    private ArrayList<HomeAppliance> homeAppliances;

    public HomeAppliancesManager(String houseManagerName) {
        this.homeManagerName = houseManagerName;
    }

    public HomeAppliancesManager(String houseManagerName, ArrayList<HomeAppliance> homeAppliances) {
        this.homeManagerName = houseManagerName;
        this.homeAppliances = homeAppliances;
    }

    public void addNewHomeApplianceToArray(HomeAppliance homeAppliance) {
        homeAppliances.add(homeAppliance);
        System.out.println("Success. Add new " + homeAppliance.getClass() + " " + homeAppliance.getName());
    }

    public void removeHomeApplianceFromArray(HomeAppliance homeAppliance)
            throws NoSuchApplianceException, ApplianceTurnedOnException {
        if (homeAppliance.isTurnedOn()) {
            throw new ApplianceTurnedOnException();
        }
        if (homeAppliances.remove(homeAppliance)) {
            System.out.println(homeAppliance.getClass() + " " + homeAppliance.getName() + " successfully removed");
        } else {
            throw new NoSuchApplianceException();
        }
    }

    public static ArrayList<HomeAppliance> findAppliancesByClass(ArrayList<HomeAppliance> homeAppliances,
                                                                 HomeAppliance applianceClass) {
        ArrayList<HomeAppliance> homeApplianceArrayList = new ArrayList<>();
        for (HomeAppliance homeAppliance : homeAppliances) {
            if (homeAppliance.getClass() == applianceClass.getClass()) {
                if (homeAppliance.isTurnedOn()==false){
                    homeApplianceArrayList.add(homeAppliance);
                }
            }
        }
        return homeApplianceArrayList;
    }

    public HomeAppliance findChosenAppliance(HomeAppliance chosenAppliance) {
        for (HomeAppliance homeAppliance : homeAppliances) {
            if (homeAppliance.equals(chosenAppliance)) {
                return homeAppliance;
            }
        }
        return null;
    }

    public ArrayList<HomeAppliance> findAllEnabledAppliances() {
        ArrayList<HomeAppliance> enabledAppliances = new ArrayList<>();
        for (HomeAppliance ha : homeAppliances) {
            if (ha.isTurnedOn()) {
                enabledAppliances.add(ha);
            }
        }
        if (enabledAppliances.size() == 0) {
            System.out.println("There is no turned ON appliances");
        }
        return enabledAppliances;
    }

    public int getPowerConsumption() {
        int consumption = 0;
        for (HomeAppliance homeAppliance : homeAppliances) {
            if (homeAppliance.isTurnedOn()) {
                consumption += homeAppliance.getConsumption();
            }
        }
        return consumption;
    }

    public String getHomeManagerName() {
        return homeManagerName;
    }

    public void setHomeManagerName(String homeManagerName) {
        this.homeManagerName = homeManagerName;
    }

    public ArrayList<HomeAppliance> getHomeAppliances() {
        if (homeAppliances.size() == 0) {
            System.out.println("There is no appliances in the house");
        }
        return homeAppliances;
    }

    public void setHomeAppliances(ArrayList<HomeAppliance> homeAppliances) {
        this.homeAppliances = homeAppliances;
    }

    @Override
    public String toString() {
        return "HomeAppliancesManager{" +
                "homeAppliances=" + homeAppliances +
                '}';
    }
}
