package com.poberezhnyk.OOP;

public class TV extends HomeAppliance {

    public TV() {
    }

    public TV(int consumption, String name) {
        super(consumption, name);
    }

    @Override
    void turnOn() {
        setTurnedOn(true);
        System.out.println("TV " + getName() + " turned on");
    }

    @Override
    void turnOff() {
        setTurnedOn(false);
        System.out.println("TV " + getName() + " turned off" );
    }

}
