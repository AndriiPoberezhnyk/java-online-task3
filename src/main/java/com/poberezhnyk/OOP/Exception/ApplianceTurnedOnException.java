package com.poberezhnyk.OOP.Exception;


public class ApplianceTurnedOnException extends Exception {
    public ApplianceTurnedOnException() {
        super("Appliance is still working. Turn it off before removing.");
    }

    public ApplianceTurnedOnException(String message) {
        super(message);
    }

    public ApplianceTurnedOnException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplianceTurnedOnException(Throwable cause) {
        super(cause);
    }
}
