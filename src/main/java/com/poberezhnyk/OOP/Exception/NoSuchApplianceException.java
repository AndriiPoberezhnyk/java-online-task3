package com.poberezhnyk.OOP.Exception;

public class NoSuchApplianceException extends Exception {
    public NoSuchApplianceException() {
        super("Appliance not found");
    }

    public NoSuchApplianceException(String message) {
        super(message);
    }

    public NoSuchApplianceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchApplianceException(Throwable cause) {
        super(cause);
    }
}
