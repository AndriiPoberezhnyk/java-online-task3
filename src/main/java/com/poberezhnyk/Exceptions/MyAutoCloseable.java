package com.poberezhnyk.Exceptions;

public class MyAutoCloseable implements AutoCloseable{
    @Override
    public void close() throws MyException {
        throw new MyException();
    }
}
